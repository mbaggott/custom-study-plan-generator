﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace custom_study_plan_generator.MetaObjects
{
    public class ExemptionModel
    {
        public string unit_code { get; set; }
        public string name { get; set; }
    }
}